import csv
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Unicode
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import requests
from baseclass import *

CSV_URL = "https://datahub.io/core/airport-codes/r/airport-codes.csv"


engine = create_engine('mysql://{user}:{password}@{server}'.
                       format(user='root', password='root', server='127.0.0.1:3306'), echo=True)
engine.execute("CREATE database if NOT EXISTS airports")
engine = create_engine('mysql://{user}:{password}@{server}/{database}'.
                       format(user='root', password='root', server='127.0.0.1:3306',
                              database='airports'), echo=True)
if not sqlalchemy.inspect(engine).has_table('airports'):
    Base.metadata.create_all(bind=engine)
else:
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")

Session = sessionmaker(bind=engine)

session = Session()
with requests.Session() as s:
    download = s.get(CSV_URL)

    decoded_content = download.content.decode('utf8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    for info in my_list:
        session.merge(Airport(info[0], info[-1]))
        session.commit()
airports = session.query(Airport).all()
#for airport in airports:
#    print(airport.id)
session.close()
