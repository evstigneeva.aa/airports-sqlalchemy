from sqlalchemy import  create_engine
from sqlalchemy.orm import sessionmaker
from baseclass import *


def search(code):
    engine = create_engine('mysql+mysqlconnector://{user}:{password}@{server}/{database}'.
                           format(user='root', password='root', server='127.0.0.1:3306',
                                  database='airports'), echo=True)
    engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")

    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(bind=engine)

    session = Session()

    #code = input()
    # airports = session.query(Airport).all()
    airport = session.query(Airport).filter_by(code).first  # ?
    #for airport in airports:
    session.close()
    #if code == airport.id:
    return airport
