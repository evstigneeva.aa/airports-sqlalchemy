from sqlalchemy import Column, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from math import *
from baseclass import *


def distance(code1, code2):
    engine = create_engine('mysql+mysqlconnector://{user}:{password}@{server}/{database}'.
                           format(user='root', password='root', server='127.0.0.1:3306',
                                  database='airports'), echo=True)
    engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")

    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(bind=engine)

    session = Session()

    #code1, code2 = input().split()
    coord1, coord2 = list(), list()
    airports = session.query(Airport).all()
    for airport in airports:
        if code1 == airport.id:
            coord1 = list(map(float, airport.coordinates.split(',')))
        elif code2 == airport.id:
            coord2 = list(map(float, airport.coordinates.split(',')))
    session.close()

    #distance

    R = 6373.0

    lat1 = radians(coord1[0])
    lon1 = radians(coord1[1])
    lat2 = radians(coord2[0])
    lon2 = radians(coord2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (sin(dlat / 2)) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2)) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return R * c
