from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Airport(Base):
    __tablename__ = 'airports'
    id = Column('id', String(50), primary_key=True)
    coordinates = Column('coordinates', String(50))  :#TODO tut sdelat listy. ili ne tut

    def __init__(self, id, coordinates):
        self.id = id
        self.coordinates = coordinates

    def __str__(self) -> str:
        return "['%s','%s']" % (self.id, self.coordinates)