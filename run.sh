#!/bin/bash
set -e

sudo apt-get install mysql-client libmysqlclient-dev python3-dev
#pyenv install $(cat .python-version)
pip install -r requirements.txt



#docker run --name=mysql1 -e MYSQL_ROOT_HOST=% --env="MYSQL_ROOT_USERNAME=root" --env="MYSQL_ROOT_PASSWORD=root" \
#-v "$(pwd)/data:/var/lib/mysql" \
#-p 3306:3306 \
#--rm \
#--detach \
#mysql:5.7 mysqld --init-connect="Grant All Privileges ON *.* to 'root'@'%' Identified By 'root';
#FLUSH PRIVILEGES;"

docker run -p 3306:3306 --rm --name contaner_name -e MYSQL_ROOT_PASSWORD=root -v "$(pwd)/data:/var/lib/mysql" mysql:5.7 mysqld --skip-grant-tables &


while ! mysqladmin ping -h "127.0.0.1" --silent; do
  echo 'ping'
  sleep 1
done
sleep 3

python ./main.py
#python ./search.py ZIA
#python ./distance.py ZIA DME
sleep 10
docker stop contaner_name